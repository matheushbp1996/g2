------

# Gerenciador de Tarefas

### Documento de Arquitetura de Software

#### Versão 1.0



| Data       | Versão | Descrição              | Autor            |
| ---------- | ------ | ---------------------- | ---------------- |
| 12/10/2020 | 1.0    | Início da documentação | Vinícius Barreto |
| 14/10/2020 | 1.0    | Ferramentas utilizadas | Victor Vidal     |
|            |        |                        |                  |
|            |        |                        |                  |



## **Node.js**



O Node.js foi escrito por Ryan Dahl no ano de 2009 mas não foi a primeira a primeira tentativa de rodar JavaScript do lado do servidor, em 1996 já havia sido criado o Netscape LiveWire Pro Web que cresceu de forma rápida com o apoio da empresa onde Dahl trabalhava, a Joyent. Segundo Dahl, a inspiração para criar o Node.js veio ao observar a barra de upload no Flickr, percebendo que o navegador perguntava a todo instante quanto do arquivo restava para ser transmitido, já que ele mesmo não tinha essa informação, e que a todo momento ficar realizando essa busca era um grande desperdício de tempo e recursos, surgiu então a tentativa de criar uma maneira mais simples de realizar isso.

Por ser uma tecnologia assíncrona (cada requisição realizada pelo próprio node não bloqueia/interrompe o processo) consegue atender a um volume bem grande de requisições ao mesmo tempo em uma única thread de execução, trabalhando ao contrário por exemplo, das linguagens de programação (vale lembrar que node.js não é uma linguagem de programação e que o informação a seguir visa comparação com intuito de agregar conhecimento) que, trabalham com multi-threading gerando uma nova thread para cada requisição que for recebida, porém, em alguns casos podem haver bloqueios da thread como em uma busca em banco de dados, liberando a execução da thread seguinte somente ao término do atual, causando lentidão.

Devido ao modelo não bloqueante de tratar requisições o Node.js se torna excelente para fazer APIs, por consumir pouquíssimo hardware, por este mesmo motivo é uma excelente ferramenta para fazer backend de jogos, apps de mensagens e IoT. Node.js não é um framework, nem uma linguagem de programação. É um ambiente para execução de código JavaScript do lado do cliente embutido em páginas HTML que rodavam em navegadores web, no entanto, o node permite utilizar JavaScript do lado servidor também para criar conteúdo web dinâmico antes que apareça no navegador para o usuário.

A arquitetura do Node.js é orientada a eventos, capaz de realizar operações de I/O assíncronas, desta maneira otimiza-se a vazão e escala das requisições nas aplicações web com muita entrada e saída ou real-time (jogos e mensageiros).

### **Instalando o Node.js**



A plataforma Node.js é distribuída de forma gratuita pelo seu mantenedor, (para baixá-lo clique [aqui](https://nodejs.org/pt-br/)) o Node.js Foundation, e é composta por:

- Um runtime JavaScript (Google V8);

- Bibliotecas para o desenvolvimento básico;

- Gerenciador de versões via linha de comando (NVM);

- Gerenciador de pacotes via linha de comando (NPM);

- Biblioteca para E/S de baixo nível (libuv);

- Utilitário REPL via linha de comando;

Após acessar o link, a versão recomendada para download é a 12.19.0 LTS (Versão estável). Durante a instalação, não é requerido nenhuma instrução especial, basta avançar até que finalize. Ao finalizar a instalação,abra o terminal de linha de comandos e digite “node -v” (sem as aspas), o resultado deve ser a versão do node instalada. Para mais informações sobre Node.js clique [aqui](https://nodejs.org/pt-br/about/).

## **Nodemon**

O Nodemon é uma ferramenta que ajuda no desenvolvimento de aplicativos baseados em node.js. Toda vez que é detectado mudanças de arquivo no diretório o aplicativo é reiniciado automaticamente, deste modo, retira do desenvolvedor a necessidade de reiniciar a aplicação sempre que houver uma mudança. Para mais informações sobre o nodemon clique [aqui](https://github.com/remy/nodemon#nodemon).



#### Instalação

A instalação do nodemon pode ser realizada de duas maneiras:

- Clonagem com git
- Usando NPM

`npm install -g nodemon`

O comando acima executa  no modo global, já criando as variáveis de ambiente. Caso queira instalar no modo dependência de desenvolvimento execute:

`npm install --save-dev nodemon`



#### Usando o nodemon

O Nodemon é executado junto a seu aplicativo para que seja possível passar todos os argumentos como normalmente faria diretamente a seu aplicativo, basta executar:

`nodemon [nome do seu app node]`

Caso esteja usando linhas de comando e precise de apoio digite:

`nodemon -h`

#### Reiniciando automaticamente

A ideia inicial do nodemon era reiniciar os processos que estivesses suspensos como por exemplo servidores web, mas agora tem a capacidade de reiniciar também aplicativos que saem de forma limpa, ou seja, sem problemas. Se seu script se enquadra nesse requisito o nodemon continuará o monitorando e o reiniciará caso apareça alguma alteração.



#### Reiniciando manualmente

Se enquanto o nodemon estiver executando, surgir a necessidade de reiniciar a aplicação, não é necessário reiniciar o nodemon, basta inserir *rs* seguido da tecla *enter* que o nodemon reiniciará o processo. 

## **Express.js**

O Express é um framework para a plataforma Node.js e linguagem JavaScript lançado como software livre e de código aberto sob a licença MIT. A ideia principal é otimizar a construção de API's e aplicações web. Foi fundado em maio de 2010 por TJ Holowaychuk. O Express é usado em todo mundo por difefentes empresas e desenvolvedores, podendo citar como exemplo Uber, Paypal e IBM. O express é bastante minimalista e mesmo assim dá aos desenvolvedores a possibilidade de criar pacotes de middleware específicos que resolvam problemas específicos que possam surgir durante o desenvolvimento de uma aplicação. O framework conta com várias bibliotecas para trabalhar com sessões, cookies, parâmetros de URL, login de usuários e outros. Para mais informações sobre o Express.js clique [aqui](https://expressjs.com/pt-br/).

Alguns dos principais recursos da estrutura express são:

- Permite configurar middlewares para responder a solicitações HTTP;
- Permite renderizar páginas HTML dinamicamente com base na passagem de argumentos para modelos;
- Integrar "view engines" para inserir dados nos templates;
- Define uma tabela de roteamento que é usada para realizar diferentes ações com base no método HTTP e URL.



#### Instalando Express.js

Para iniciar a instalação, iremos executar o comando abaixo para instalar globalmente o Express utilizando o NPM para que possa ser usada pra criar uma API ou aplicação web utilizando node.

`npm install express --save`

É importante que os módulos abaixo também sejam instalados:

**body-parser** Middleware node.js usado para lidar com dados de formulários codificados em JSON, Raw, texto e URL.

`npm install body-parser --save`

**cookie-parser** Analisa o cabeçalho do cookie e preenche os requerimentos com um objeto codificado pelos nomes dos cookies.

`npm install cookie-parser --save`

**multer** Middleware node.js usado para fazer upload e compressão de arquivos

`npm install multer --save`