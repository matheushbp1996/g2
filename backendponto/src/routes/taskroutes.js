const express = require('express');
const router = express.Router();

const TaskControl = require('../controller/taskcontrol');
const TaskValidation = require('../middleware/taskvalidacao');
const macaddressvalidacao = require('../middleware/macaddressvalidacao');


router.post('/', TaskValidation, TaskControl.create);
router.put('/:id', TaskValidation, TaskControl.update);
router.get('/filter/all', macaddressvalidacao,  TaskControl.all);
router.get('/:id', TaskControl.show);
router.delete('/:id', TaskControl.delete);
router.put('/:id/:done', TaskControl.done);
router.get('/filter/late', macaddressvalidacao, TaskControl.late); // FILTRAR ATRASADOS
router.get('/filter/today', macaddressvalidacao, TaskControl.today);// FILTRAR FUNCIONARIOS 
router.get('/filter/week', macaddressvalidacao, TaskControl.week);

//NÃO VAI TER O FILTRO DE MES E ANO?

module.exports = router;
