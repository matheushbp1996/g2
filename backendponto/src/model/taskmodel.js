const mongoose = require('../config/bancodados');

const Schema = mongoose.Schema;

const taskSchema = new Schema({
    macaddress: {type: String, required: true},
    type: {type: Number, required:true},
    title: {type: String, required: true}, 
    description: {type: String, required: true},
    when: {type: Date, required: true},
    done: {type: Boolean, default: false},
    created: {type: Date, default: Date.now } //IMPORTANTE!! MOMENTO EM QUE O FUNCIONARIO ENTROU NO SERVIÇO
});

module.exports = mongoose.model('task', taskSchema);
